\contentsline {chapter}{Introduction}{3}
\select@language {french}
\contentsline {chapter}{\numberline {1}Plan de la lettre}{4}
\contentsline {chapter}{\numberline {2}Types de lettres de motivation}{4}
\contentsline {section}{\numberline {2.1}Lettre de motivation en réponse à des petites annonces}{4}
\contentsline {section}{\numberline {2.2}Lettres de motivation en candidature spontanée}{5}
\contentsline {section}{\numberline {2.3}Lettre de motivation envoyée aux cabinets de recrutement :}{5}
\contentsline {section}{\numberline {2.4}Candidature par téléphone}{6}
\contentsline {chapter}{\numberline {3}Types de formules}{7}
\contentsline {section}{\numberline {3.1}Proposition de candidature}{7}
\contentsline {section}{\numberline {3.2}Formation}{8}
\contentsline {section}{\numberline {3.3}Expériences professionnelles}{8}
\contentsline {section}{\numberline {3.4}Annonce du CV}{9}
\contentsline {section}{\numberline {3.5}Formules de conclusion}{10}
\contentsline {section}{\numberline {3.6}Formules de politesse}{10}
\contentsline {chapter}{\numberline {4}Quelques erreurs à éviter}{11}
\contentsline {section}{\numberline {4.1}Erreurs de forme}{11}
\contentsline {section}{\numberline {4.2}Erreurs de fond}{11}
\contentsline {chapter}{Conclusion}{12}
\contentsline {chapter}{Bibliographie}{13}
\contentsline {chapter}{Webographie}{13}
